# An empty student report
This report was created by students following Software Engineering (Bachelor and Masters) on [Aalborg University](www.cs.aau.dk).

The empty report is build over four years, from 2010 to 2014, and contains a lot of small things that we, the authors, feel is important to have in a report.

## Usage
The empty report should be rather straight forward to use, most "empty" fields are marked with EMPTY, and require filling out.

The regular version of the report is set up to be compiled using `latexmk -pdf master.tex`.

We do however recommend that you use the LaTeX Makefile found [here](https://code.google.com/p/latex-makefile/) as it includes conversion from svg to pdf and other nice automation features.
